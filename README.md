# Quotes - a program for quoting things

It's really dumb I basically I just wanted to learn go.

How to use:

- Copy the example file (test.quotes) to /home/\<user\>/.config/quotes, XDG var is default
- Add your own quotes
- Blank lines indicate the end of a quote, include an author or the program will hate you
- To add an author use the - Name syntax
- Comments can ONLY be on a standalone line (no inline)
- a space before the line will get removed, you can use this to escape a # at the beginning of a line

License is Unlicense, see LICENSE for more details
