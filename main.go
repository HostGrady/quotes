package main

import (
	"fmt"
	"os"
	"bufio"
	"regexp"
	"errors"
	"math/rand"
	"time"
)

// TYPES

type Quote struct {
	Author string
	Text string 
}

// FUNCS

// PRIVATE
func check(e error) {
	if e != nil {
		panic(e)
	}
}

func quotify(q Quote) string { 
	return fmt.Sprintf("\"%s\" - %s", q.Text, q.Author) 
}

func quoteBlockToQuote(quoteBlock string) (Quote, error) {
	authorRegex := regexp.MustCompile("(\\w+)[^-]*$")
	removeAuthorRegex := regexp.MustCompile("\\s(\\S+)[^-]*$")

	author := authorRegex.FindAllString(quoteBlock, 1)
	text := removeAuthorRegex.ReplaceAllString(quoteBlock, "")

	if text == "" || author[0] == quoteBlock {
		return Quote{"", "",}, errors.New("Not a quote")
	}

	q := Quote{Author: author[0], Text: text,}
	return q, nil
}

func getQuotesFromFile(fileName string) []Quote {
	f, err := os.Open(fileName)
	check(err)
	defer f.Close()

	fs := bufio.NewScanner(f)
	qb := ""
	quotesArray := []Quote{}


	for fs.Scan() {
		line := fs.Text()
		
		if len(line) == 0 && len(qb) != 0 {
			quote, err := quoteBlockToQuote(string(qb[:len(qb)-1]))
			check(err)
			quotesArray = append(quotesArray, quote)
			qb = ""
			continue
		} else if len(line) == 0 && len(qb) == 0 {
			continue
		}

		// comments
		if string(line[0]) == "#" {
			continue
		}

		// "escaping" comments
		if string(line[0]) == " " {
			line = string(line[1:])
		}

		qb += line + "\n"
	}

	if qb != "" {
		quote, err := quoteBlockToQuote(string(qb[:len(qb)-1]))
		check(err)
		quotesArray = append(quotesArray, quote)
		qb = ""
	}

	return quotesArray

}

func getRandQuote(quotes []Quote, author string) Quote {
	rand.Seed(time.Now().UnixNano()) 
	var randomNumber int
	var randomQuote Quote
	var quotesFromAuthor []Quote

	if author == "" {
		randomNumber = rand.Intn(len(quotes))
		randomQuote = quotes[randomNumber]
	} else {
		for _, quote := range quotes {
			if quote.Author != author {
				continue
			}

			quotesFromAuthor = append(quotesFromAuthor, quote)
		}

		if len(quotesFromAuthor) == 0 {
			fmt.Println("You need to use a real author")
			os.Exit(1)
		}

		randomNumber = rand.Intn(len(quotesFromAuthor))
		randomQuote = quotesFromAuthor[randomNumber]
	}
	
	return randomQuote

}

// MAIN (CLI INTERFACE)
func main() {
	c, err := os.UserConfigDir()
	check(err)

	cfgPath := c + "/quotes"
	author := ""

	if len(os.Args[1:]) > 1 {
		for _, v := range os.Args[1:] {
			author += v + " "
		}
		author = string(author[:len(author)-1])
	} else if len(os.Args[1:]) == 1 {
		author = os.Args[1]
	}

	quotes := getQuotesFromFile(cfgPath)
	randQuote := getRandQuote(quotes, author)
	fmt.Println(quotify(randQuote))
}
